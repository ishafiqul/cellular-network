import sklearn
import joblib
import numpy as np
import random
import xgboost as xgb
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.cluster import MeanShift
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import Birch
from sklearn.cluster import DBSCAN
from sklearn.cluster import MiniBatchKMeans
from sklearn.mixture import GaussianMixture
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from scipy.stats import spearmanr
from scipy.cluster import hierarchy
from collections import defaultdict
from scipy.spatial.distance import squareform

random.seed(40)
mpl.rcParams["figure.figsize"] = (12, 10)
colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]


class CellularModel(object):
    """
    This is model training class specially designed for cellular
    network dataset.


    Methods:
    -------

    prepare_model_data(self, df)
        It prepare given model training data.

    split_test_train_data(self, df, test_size = 0.3)
        Splits test and train dataframe.

    initiate_xgb_model(hyper_param_dict)
        Initiates an xgboost model object using given hyperparameters.

    tune_byperparams(df, param_hyperoptRSCV)
        Provides tuned hyperparameters for XGBoost classifer by retuning

    train_model(df, hyper_param_dict, overfit_control=True)
        Train model using xgboost algorith

    load_model(self, model_file_name='xgb_model.pkl')
        Loads pretrain model from working directory

    perform_cross_validation(df, hyper_param_dict)
        Performs 10 folds cross validation using full dataset
        (test and train dataset)

    get_auc(model, X_,y_)
        Calculates AUC for a given model and given dataset

    plot_roc(name, y_true, y_probs, **kwargs)
        Reads a file (csv, pickle, pkl) from s3

    select_prob_cutoff(self, y_true, y_probs, plot=False)
        It defines probability cutoff to classify. The optimal cutoff would be
        where tpr is high and fpr is low and tpr - (1-fpr) is zero or near to
        zero. In the other word,where sensitivity and specificity are almost
        equal.

    """

    def __init__(self):
        """
        Attributes
        ----------
        hyperparatmers_to_tune : dict
            Dict to define hyperparameters ranges
        """

        self.hyperparatmers_to_tune = {
            "learning_rate": [0.2, 0.3, 0.4],
            "n_estimators": [int(x) for x in np.linspace(start=1500, stop=3000, num=5)],
            "max_depth": [i for i in range(6, 10)],
            "gamma": [i for i in range(0, 5)],
            "reg_lambda": [i * 0.1 for i in range(0, 5)],
            "colsample_bytree": [i / 10.0 for i in range(6, 11)],
        }

    def prepare_model_data(self, df):

        """
        Attributes
        ----------
        df : dataframe
            Essential cleaning has been done in this method. This is
            very specific to this dataset

        Return:
        -----------
        df : dataframe
            Clean dataset
        """
        df = df.rename(columns={"maxUE_UL+DL": "maxUE_UL_DL"})
        df[df["maxUE_UL_DL"] == "#¡VALOR!"] = np.nan
        df.fillna(method="pad", inplace=True)
        # df['maxUE_UL_DL']=df['maxUE_UL_DL'].fillna(method='pad', inplace=True)
        df["maxUE_UL_DL"] = df["maxUE_UL_DL"].astype("float64")
        df = df.drop(["Time", "CellName"], axis=1)

        return df

    def split_test_train_data(self, df, test_size=0.3):

        """
        Attributes
        ----------
        df : dataframe
            Clean and curated dataset
        test_size : float
                Define fraction of test dataset

        Return:
        -----------
        X_train, X_test, y_train, y_test : dataframes
            Splitted dataset

        """

        x_cols = [c for c in df.columns if c not in ["Unusual"]]
        # set input matrix and target column
        X = df[x_cols]
        y = df["Unusual"]
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=test_size, random_state=1234
        )

        return X_train, X_test, y_train, y_test

    def initiate_xgb_model(self, hyper_param_dict=""):
        """
        Initiate an xgboost model object using given hyperparameters. In case, no
        hyperparameters defination, it will use default hyperparameters

        Parameters:
        -----------

        hyper_param_dict : dict,
            tuned hyperparameters

        Return:
        -----------
        model : pkl
            Model object

        Notes:
        -----------
        1. It just initiate a model object

        """

        if hyper_param_dict == "":
            model = xgb.XGBClassifier(
                objective="binary:logistic", random_state=42, n_jobs=-1, verbosity=0
            )
        else:
            model = xgb.XGBClassifier(
                gamma=hyper_param_dict["gamma"],  # 10 is best for ATL verified
                min_child_weight=hyper_param_dict["min_child_weight"],
                booster=hyper_param_dict["booster"],
                colsample_by_tree=hyper_param_dict["colsample_bytree"],
                learning_rate=hyper_param_dict["learning_rate"],
                max_depth=hyper_param_dict["max_depth"],
                n_estimators=hyper_param_dict["n_estimators"],
                reg_lambda=hyper_param_dict["reg_lambda"],
                verbosity=0,
            )
        return model

    def tune_hyperparams(self, df, hyperparatmers_to_tune, retune=False):

        """
        Attributes
        ----------
        hyperparatmers_to_tune : dict
            Dict to define hyperparameters ranges
        df : dataframe
            Clean dataset
        retune : bool
            Wheather to retune or not

        Return:
        -----------
        best_param : dict,
            Dictionary of trained parameters.



        """

        if retune:

            cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=42)
            xb = xgb.XGBClassifier(
                objective="binary:logistic", random_state=42, n_jobs=-1
            )

            rgs = RandomizedSearchCV(
                estimator=xb,
                param_distributions=hyperparatmers_to_tune,
                n_iter=20,
                cv=cv,
                random_state=42,
                n_jobs=-1,
                scoring="roc_auc",
                return_train_score=True,
            )

            X_train = df[df.columns[0:-1]].values
            y_train = df[df.columns[-1]].values
            rgs.fit(X_train, y_train)

            best_param = rgs.best_estimator_.get_params()

            file_name_str = "xgb_tuned_best_param.pkl"
            joblib.dump(best_param, file_name_str)

        else:
            print("####Using Previously trained hyperparatmer###########")
            file_name_str = "xgb_tuned_best_param.pkl"
            best_param = joblib.load(file_name_str)
            print("hyper_param_dict has been loaded from {} ".format(file_name_str))

        print("Tuned Hyperparameters :", best_param)

        return best_param

    def train_model(
        self,
        df,
        hyper_param_dict="",
        model_file_name="xgb_model.pkl",
        model_overfit_control=True,
    ):
        """
        Provides a trained model either by retraining or reloading previously
        trainned models

        Parameters:
        -----------

        hyper_param_dict : dict,
            tuned hyperparameters

         X_train, y_train, X_test, y_test: Splitted train and test X and y

        Return:
        -----------
        model : pkl
            Model object

        Notes:
        -----------
        1. Specially designed for xgboost model


        """

        X_train, X_test, y_train, y_test = self.split_test_train_data(df, test_size=0.3)

        model = self.initiate_xgb_model(hyper_param_dict)

        if model_overfit_control:
            eval_set = [(X_test, y_test)]
            model.fit(
                X_train,
                y_train,
                early_stopping_rounds=10,
                eval_metric="logloss",
                eval_set=eval_set,
                verbose=True,
            )
        else:
            model.fit(X_train, y_train)
            print("Done...")

            # joblib.dump(model, model_file_name)

        with open(model_file_name, "wb") as handle:
            joblib.dump(model, handle)

        return model

    def load_model(self, model_file_name="xgb_model.pkl"):

        """
        Loads pretrained model

        Parameters:
        -----------
        model_file_name : str,
            model file path

        Return:
        -----------
        model : pkl
            Model object

        """

        loaded_model = joblib.load(model_file_name)
        print("Pretrain model has been loaded from {} ".format(model_file_name))
        return loaded_model

    def get_model(
        self,
        df,
        hyper_param_dict="",
        model_file_name="xgb_model.pkl",
        model_overfit_control=True,
        load=True,
    ):

        """
        trains new model or Loads pretrained model

        Parameters:
        -----------
        df : dataframe,
        pandas dataframe containing model build and business
        validation data.

        hyper_param_dict : dict,
        tuned or selected individual hyperparameters

        model_file_name : str,
            model file path

        Return:
        -----------
        model : pkl
            Model object

        """

        if load:
            model = self.load_model(model_file_name=model_file_name)
        else:
            model = self.train_model(
                df,
                hyper_param_dict="",
                model_overfit_control=True,
                model_file_name=model_file_name,
            )

    def perform_cross_validation(self, df, hyper_param_dict):
        """
        Performs 10 folds cross validation using full dataset (test and
        train datasets)

        Parameters:
        -----------
        df : dataframe,
            pandas dataframe containing full dataset (test and train datasets)

        hyper_param_dict : dict,
            tuned hyperparameters

        Return:
        -----------
        cv_scores : list
            Returns scores of cross validation

        Notes:
        -----------
        1. It just initiates a model object

        """

        X = df[df.columns[0:-1]]
        y = df[df.columns[-1]]
        model = self.initiate_xgb_model(hyper_param_dict)

        skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=30)
        cv_scores = cross_val_score(model, X, y, scoring="roc_auc", cv=skf)

        return list(cv_scores)

    def get_auc(self, model, X_, y_):
        """
        Calculates AUC for a given model and given dataset

        Parameters:
        -----------
        model : model obj,
            Model object to calculate probability
        X_ : Array
            Model Feature set
        model : model obj,
            Target dataset


        Return:
        -----------
        auc_ : float
            Returns AUC of model for a given dataset
        probs_ : Array
            Array of probs for a given model and given dataset

        Notes:
        -----------
        1. OOT data has been prepared in preprocessing class.
        """

        probs_ = model.predict_proba(X_)
        probs_ = probs_[:, 1]
        auc_ = roc_auc_score(y_, probs_)
        return auc_, probs_

    def plot_roc(self, y_true, y_probs, name, **kwargs):
        """
        Plots ROC curve

        Parameters:
        -----------
        name : str,
            Name of the curve
        y_true : Array
            Actual Target
        y_probs : Array,
            Model probability for target

        Return:
        -----------
        plt : object

        """

        fp, tp, _ = sklearn.metrics.roc_curve(y_true, y_probs)
        fig = plt.figure(figsize=(12, 10))
        plt.plot(100 * fp, 100 * tp, label=name, linewidth=2, color="#FFC300", **kwargs)
        plt.plot([0, 100], [0, 100], color="#900C3F", linewidth=1, linestyle="--")
        plt.xlabel("False positives [%]")
        plt.ylabel("True positives [%]")
        plt.xlim([-0.5, 100])
        plt.ylim([0, 100.5])
        plt.grid(False)
        ax = plt.gca()
        ax.set_aspect("0.75")
        return plt

    def select_prob_cutoff(self, y_true, y_probs, plot=False):
        """
        It defines probability cutoff to classify. The optimal cutoff would be
        where tpr is high and fpr is low and tpr - (1-fpr) is zero or near to
        zero. In the other word,where sensitivity and specificity are almost
        equal.

        Parameters:
        -----------
        y_true : array,
            True value of target.
        y_probs : array,
            probability of the target
        reg : str,
            model region to decide which model to use for prediction.
        plot : bool,
            In case of true value, it also plots Sensitivity and Specificity
            plots togther to visulize cutoff point.
        Return:
        -----------
        list(roc_t['threshold']): List
            A list of single value containing single value of threshold

        """

        fpr, tpr, threshold = roc_curve(y_true, y_probs)
        i = np.arange(len(tpr))
        roc = pd.DataFrame(
            {
                "tf": pd.Series(tpr - (1 - fpr), index=i),
                "threshold": pd.Series(threshold, index=i),
            }
        )
        roc_t = roc.loc[(roc.tf - 0).abs().argsort()[:1]]

        if plot == True:
            png_path = "Threshold_ROC.png"

            roc["fpr"] = pd.Series(fpr, index=i)
            roc["tpr"] = pd.Series(tpr, index=i)
            roc["1-fpr"] = pd.Series(1 - fpr, index=i)
            fig, ax = plt.subplots()
            plt.plot(roc["tpr"], label="tpr")
            plt.plot(roc["1-fpr"], label="1-fpr")
            plt.legend(loc="best")
            plt.xlabel("1-False Positive Rate")
            plt.ylabel("True Positive Rate")
            plt.title("Receiver Operating Characteristics")
            plt.tight_layout()
            plt.savefig(png_path, dpi=250)

        return list(roc_t["threshold"])

    def make_boxplot(self, df, ncols=6, nrows=2):

        """
        Plots boxplot

        Parameters:
        -----------
        df : dataframe,
            pandas dataframe containing full dataset (test and train datasets)

        Return:
        -----------
        plt : object

        """

        fig, axs = plt.subplots(ncols=ncols, nrows=nrows, figsize=(20, 10))
        index = 0
        axs = axs.flatten()
        for k, v in df.items():
            sns.boxplot(y=k, data=df, ax=axs[index], palette="Set3")
            index += 1
        plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=5.0)

    def make_heatmap(self, df, color_map="YlOrRd"):

        """
        Plots heatmap

        Parameters:
        -----------
        df : dataframe,
            pandas dataframe containing full dataset (test and train datasets)

        Return:
        -----------
        plt : object

        """

        plt.figure(figsize=(20, 10))
        sns.heatmap(df.corr().abs(), cmap=color_map, annot=True)
        plt.show()

    def plot_confusion_matrix(self, y_true, probs_pred, basis="prob", p=0.5):
        """
        Plots confusion_matrix and related values

        Parameters:
        -----------
        y_true : array or list
            true value or ground truth,
        probs_pred : array or list
            predicted probability or predicted values

        basis: str
            flag Whether it is probability array or prediction

        Return:
        -----------
        plt : object

        """

        if basis == "prob":
            cm = confusion_matrix(y_true, probs_pred > p)
            predictions = [0 if prob < p else 1 for prob in probs_pred]
        else:
            n_class=len(list(set(probs_pred)))
            if n_class>2:
                probs_pred=[0 if v ==0 else 1 for v in probs_pred]
            cm = confusion_matrix(y_true, probs_pred)
            predictions = probs_pred

        plt.figure(figsize=(5, 5))
        sns.heatmap(cm, annot=True, cmap="YlOrRd", annot_kws={"size": 16})  # font size
        plt.title("Confusion matrix")
        plt.ylabel("Actual label")
        plt.xlabel("Predicted label")
        print("######################################################")
        print("######################################################")
        print("Legitimate Normal Condition (True Negatives): ", cm[0][0])
        print("Legitimate Normal Condition Missed (False Positives): ", cm[0][1])
        print("Unusual Condition Missed (False Negatives): ", cm[1][0])
        print("Unusual Condition Identified (True Positives): ", cm[1][1])
        print("Total Unusual Conditions : ", np.sum(cm[1]))
        print("######################################################")
        print("######################################################")

        recall = round(recall_score(y_true, predictions), 3)
        precision = round(precision_score(y_true, predictions), 3)
        f1 = round(f1_score(y_true, predictions), 3)
        print("Recall Score of the classifier:{}".format(recall))
        print("Precision Score of the classifier:{}".format(precision))
        print("F1 Score of the classifier:{}".format(f1))

        print("######################################################")
        print("######################################################")

    def remove_multicollinear_features(self, df, threshold=0.5, plot_dendrogram=True):
        """
        Remove multicollinear features  and plot dendrogram
        Parameters:
        -----------
        df : dataframe,
        pandas dataframe containing model build and business
        validation data.

        Return:
        -----------
        df : dataframe,
        pandas dataframe after removing multicollinear features for model
        development.
        threshold: float, optional and default value is 0.5
        For criteria ‘inconsistent’, ‘distance’ or ‘monocrit’, this
        is the threshold to apply when forming flat clusters.

        plot_dendrogram: bool
        wheather to plot a dendrogram or not


        Notes:
        -----------
        1. Prior to analysis null has been handled using '0'.

        """

        X = df[df.columns[0:-1]]
        corr = X.corr()
        corr = corr.fillna(0)
        corr = np.array(corr)
        corr_linkage = hierarchy.ward(corr)

        cluster_ids = hierarchy.fcluster(corr_linkage, threshold, criterion="distance")

        cluster_id_to_feature_ids = defaultdict(list)

        for idx, cluster_id in enumerate(cluster_ids):
            cluster_id_to_feature_ids[cluster_id].append(idx)

        selected_features = [v[0] for v in cluster_id_to_feature_ids.values()]
        sel_col = X.columns[selected_features]

        if plot_dendrogram:
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
            dendro = hierarchy.dendrogram(
                corr_linkage, labels=X.columns, ax=ax1, leaf_rotation=90
            )

            dendro_idx = np.arange(0, len(dendro["ivl"]))
            ax2.imshow(corr[dendro["leaves"], :][:, dendro["leaves"]])
            ax2.set_xticks(dendro_idx)
            ax2.set_yticks(dendro_idx)
            ax2.set_xticklabels(dendro["ivl"], rotation="vertical")
            ax2.set_yticklabels(dendro["ivl"])
            fig.tight_layout()
            plt.show()

        df_features = list(sel_col) + [df.columns[-1]]
        df = df[df_features]

        return df

    def select_unsupervised_model(self, model_name="km"):

        """
        Select an unsupervised/ clustering algorith

        Parameters:
        -----------
        model_name : str,
            defines which algorithm is selected.
            follwings are the available options:
                km=KMeans
                ac=AgglomerativeClustering
                ms=MeanShift
                mb=MiniBatchKMeans
                gm=GaussianMixture

        Return:
        -----------
        model : object
            Returns model object

        Notes:
        -----------
        1. It just initiates a model object

        """


        if model_name == "ms":
            model = MeanShift()
            print("MeanShift clustering unsupervised model has been selected.")
        elif model_name == "mb":
            model = MiniBatchKMeans(n_clusters=2)
            print("MiniBatchKMeans clustering unsupervised model has been selected.")
        elif model_name == "km":
            model = KMeans(n_clusters=2)
            print("KMeans clustering unsupervised model has been selected.")
        elif model_name == "gm":
            model = GaussianMixture(n_components=2)
            print("GaussianMixture clustering unsupervised model has been selected.")
        else:
            print("Unknow model has been selected")
        return model

    def calculate_confusion_matrix(self, y_true, y_probs, prob_cutoff_test):
        """
        Calculate Confusion Matrix

        Parameters:
        -----------
        name : str,
            Name of the curve
        y_true : Array
            Actual Target
        y_probs : Array,
            Model probability for target
        prob_cutoff_test : float,
            Porbability for cuttoff
        Return:
        -----------
        plt : object

        """

        y_pred = [0 if v <= prob_cutoff_test else 1 for v in y_probs]
        cm = confusion_matrix(y_true, y_pred)
        return cm

    def ABS_SHAP(self, df_shap, df, n_best_fs_2_plot):
        """
        Attributes
        ----------
        df_shap : dataframe
            Shape Values
        df : dataframe
            Data to evaluate SHAP Values

        Return:
        -----------
        k2 : dataframe
            Dataframe with SHAP Inportance

        """
        fig = plt.figure(figsize=(12, 10))
        shap_v = pd.DataFrame(df_shap)
        feature_list = df.columns.tolist()
        shap_v.columns = feature_list
        df_v = df.copy().reset_index()

        # Determine the correlation in order to plot with different colors
        corr_list = list()
        for i in feature_list:
            b = np.corrcoef(shap_v[i], df_v[i])[1][0]
            corr_list.append(b)
        corr_df = pd.concat(
            [pd.Series(feature_list), pd.Series(corr_list)], axis=1
        ).fillna(0)
        # Make a data frame. Column 1 is the feature, and Column 2 is the correlation coefficient
        corr_df.columns = ["Variable", "Corr"]
        corr_df["Sign"] = np.where(corr_df["Corr"] > 0, "red", "blue")

        # Plot it
        shap_abs = np.abs(shap_v)
        k = pd.DataFrame(shap_abs.mean()).reset_index()
        k.columns = ["Variable", "SHAP_abs"]
        k2 = k.merge(corr_df, left_on="Variable", right_on="Variable", how="inner")
        k2 = k2.sort_values(by="SHAP_abs", ascending=True)[-n_best_fs_2_plot:]
        colorlist = k2["Sign"]
        ax = k2.plot.barh(
            x="Variable", y="SHAP_abs", color=colorlist, figsize=(5, 6), legend=False
        )
        ax.set_xlabel("SHAP Value (Red = Positive Impact)")

        return k2

    def generate_and_save_final_results(self, model, df, prob_cutoff_test):

        """
        Generate results for submission

        Parameters:
        -----------
        model : object,
            Trained Final Model

        df : dataframe
            Data for evaluation without lebel (clean data)
        prob_cutoff_test : float,
            Porbability for cuttoff

        Return:
        -----------
        df : dataframe
            data with id and prediction labels

        """

        probs = model.predict_proba(df.values)
        probs_ = probs[:, 1]
        y_pred = [0 if v <= prob_cutoff_test else 1 for v in probs_]
        df2 = pd.DataFrame()
        df2["Label"] = y_pred
        df2["Id"] = df2.index + 1
        df2 = df2[["Id", "Label"]]

        df2.to_csv("prediction.csv", index=False)
        return df2


def main_model_train(given_data_file_name):
    """
    This is final function that combines all model training and evaluation
    functions.

    Parameter(s)
    ----------
        None

    Notes:
    -----------
     There is always ways to do it better.

    """

    t1 = datetime.now()
    mt = CellularModel()
    df = pd.read_csv(given_data_file_name, encoding="unicode_escape")
    df = mt.prepare_model_data(df)
    X_train, X_test, y_train, y_test = mt.split_test_train_data(df, test_size=0.3)
    param_hyperoptRSCV = mt.param_hyperoptRSCV
    # hyper_param_dict=mt.tune_byperparams(df, param_hyperoptRSCV)
    hyper_param_dict = ""
    model = mt.train_model(df, hyper_param_dict)
    auc_train, probs_train = mt.get_auc(model, X_train, y_train)
    prob_cutoff_train = mt.select_prob_cutoff(y_train, probs_train)
    auc_test, probs_test = mt.get_auc(model, X_test, y_test)
    prob_cutoff_test = mt.select_prob_cutoff(y_test, probs_test)
    cm = mt.calculate_confusion_matrix(y_test, probs_test, prob_cutoff_test)
    print(cm)

    model_final_results = {}

    # Model Summary results
    model_final_results["auc_train"] = auc_train
    model_final_results["auc_test"] = auc_test
    model_final_results["threshold_train"] = prob_cutoff_train[0]
    model_final_results["threshold_test"] = prob_cutoff_test[0]

    print(model_final_results)
    print("##########################################")

    t2 = datetime.now()
    total_req = t2 - t1
    print("Calculation Time", total_req)


if __name__ == "__main__":
    given_data_file_name = "ML-MATT-CompetitionQT1920_train.csv"
    main_model_train(given_data_file_name)
